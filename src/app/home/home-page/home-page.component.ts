import { Component } from '@angular/core';
import { Store, select } from '@ngrx/store';

import { IState, getPosts } from 'src/app/store/reducer';
import { GetPosts } from 'src/app/store/actions';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css'],
})
export class HomePageComponent {
  posts$ = this.store.pipe(select(getPosts));

  constructor(private store: Store<IState>) {
    this.store.dispatch(new GetPosts());
  }
}
