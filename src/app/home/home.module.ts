import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { HomePageComponent } from './home-page/home-page.component';
import { ShortPostComponent } from './short-post/short-post.component';
import { NavigationComponent } from './navigation/navigation.component';
import { SharedModule } from '../shared/shared.module';
import { homeRoutes } from './home-routing.module';

@NgModule({
  imports: [CommonModule, SharedModule, RouterModule.forChild(homeRoutes)],
  declarations: [HomePageComponent, ShortPostComponent, NavigationComponent],
  exports: [HomePageComponent, ShortPostComponent, NavigationComponent],
})
export class HomeModule {}
