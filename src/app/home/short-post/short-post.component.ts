import { Component, Input } from '@angular/core';
import { Store } from '@ngrx/store';

import { IPost } from 'src/app/store/effects';
import { IState } from 'src/app/store/reducer';
import { ShowOnePostDetailed } from 'src/app/store/actions';

@Component({
  selector: 'app-short-post',
  templateUrl: './short-post.component.html',
  styleUrls: ['./short-post.component.css'],
})
export class ShortPostComponent {
  @Input()
  post: IPost;

  constructor(private store: Store<IState>) {}

  onClick(): void {
    this.store.dispatch(new ShowOnePostDetailed({ postUrl: this.post.url }));
  }
}
