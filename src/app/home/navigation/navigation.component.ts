import { Component } from '@angular/core';
import { Store, select } from '@ngrx/store';

import { getCurrentPage, getLastPage } from 'src/app/store/reducer';
import { SetCounter } from 'src/app/store/actions';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css'],
})
export class NavigationComponent {
  currentPage$ = this.store.pipe(select(getCurrentPage));
  isLastPage$ = this.store.pipe(select(getLastPage));
  isFirstPage = true;

  constructor(private store: Store<any>) {
    this.checkPage();
  }

  OnSetCounter(action: 1 | -1): void {
    this.store.dispatch(new SetCounter({ action }));
  }

  checkPage(): void {
    this.currentPage$.subscribe(cp => (this.isFirstPage = cp === 1));
  }
}
