import { Actions, Effect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { map, tap, switchMap } from 'rxjs/operators';

import {
  GetPosts,
  AppActions,
  SavePosts,
  ShowOnePostDetailed,
} from './actions';
import { IState } from './reducer';
import { HttpService } from '../http.service';

export interface IPost {
  title: string;
  author: string;
  date: Date;
  url: string;
  text?: string;
  img?: string;
}

@Injectable()
export class AppEffects {
  @Effect()
  getPosts$ = this.actions$.pipe(
    ofType<GetPosts>(AppActions.GET_POSTS),
    switchMap(() =>
      this.http.fetchTodos().pipe(
        map(
          posts =>
            new SavePosts({
              posts: posts.map((post: any) => ({
                title: post.title,
                author: post.author,
                date: new Date(post.created),
                text: post.selftext,
                url: post.url,
                img: post.thumbnail,
              })),
            })
        )
      )
    )
  );

  @Effect({ dispatch: false })
  showOnePost$ = this.actions$.pipe(
    ofType<ShowOnePostDetailed>(AppActions.SHOW_ONE_POST_DETAILED),
    tap(data =>
      this.router.navigate(['post'], {
        queryParams: { postUrl: data.payload.postUrl },
      })
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store<IState>,
    private router: Router,
    private http: HttpService
  ) {}
}
