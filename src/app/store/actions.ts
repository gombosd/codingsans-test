import { IPost } from './effects';

export enum AppActions {
  GET_POSTS = 'get posts',
  SET_COUNTER = 'set counter',
  SAVE_POSTS = 'save posts',
  SHOW_ONE_POST_DETAILED = 'show one post detailed',
}

export class GetPosts {
  readonly type = AppActions.GET_POSTS;
  constructor() {}
}

export class SetCounter {
  readonly type = AppActions.SET_COUNTER;
  constructor(public payload: { action: 1 | -1 }) {}
}

export class SavePosts {
  readonly type = AppActions.SAVE_POSTS;
  constructor(public payload: { posts: IPost[] }) {}
}

export class ShowOnePostDetailed {
  readonly type = AppActions.SHOW_ONE_POST_DETAILED;
  constructor(public payload: { postUrl: string }) {}
}

export type AppAction = GetPosts | SetCounter | SavePosts | ShowOnePostDetailed;
