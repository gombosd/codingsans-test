import { createSelector } from '@ngrx/store';
import { IPost } from './effects';
import { AppAction, AppActions } from './actions';

const dummyPosts: IPost[] = [
  {
    title: 'test title 1',
    author: 'test author',
    date: new Date(),
    text: 'test text',
    url: 'test url',
  },
  {
    title: 'test title 2',
    author: 'test author',
    date: new Date(),
    text: 'test text',
    url: 'test url',
  },
  {
    title: 'test title 3',
    author: 'test author',
    date: new Date(),
    text: 'test text',
    url: 'test url',
  },
  {
    title: 'test title 4',
    author: 'test author',
    date: new Date(),
    text: 'test text',
    url: 'test url',
  },
  {
    title: 'test title 5',
    author: 'test author',
    date: new Date(),
    text: 'test text',
    url: 'test url',
  },
  {
    title: 'test title 6',
    author: 'test author',
    date: new Date(),
    text: 'test text',
    url: 'test url',
  },
  {
    title: 'test title 7',
    author: 'test author',
    date: new Date(),
    text: 'test text',
    url: 'test url',
  },
  {
    title: 'test title 8',
    author: 'test author',
    date: new Date(),
    text: 'test text',
    url: 'test url',
  },
  {
    title: 'test title 9',
    author: 'test author',
    date: new Date(),
    text: 'test text',
    url: 'test url',
  },
  {
    title: 'test title 10',
    author: 'test author',
    date: new Date(),
    text: 'test text',
    url: 'test url',
  },
  {
    title: 'test title 11',
    author: 'test author',
    date: new Date(),
    text: 'test text',
    url: 'test url',
  },
  {
    title: 'test title 12',
    author: 'test author',
    date: new Date(),
    text: 'test text',
    url: 'test url',
  },
];

export interface IState {
  posts: IPost[];
  currentPage: number;
}

const initialState = {
  posts: dummyPosts,
  currentPage: 0,
};

export function appReducer(state: IState = initialState, action: AppAction) {
  switch (action.type) {
    case AppActions.SET_COUNTER: {
      const pageAction = action.payload.action;
      state.currentPage += pageAction;
      return { ...state };
    }
    case AppActions.SAVE_POSTS: {
      const posts = action.payload.posts;
      state.posts = posts;
      return { ...state };
    }
    default:
      return state;
  }
}

export const gameState = state => state.app as IState;

export const getCurrentPage = createSelector(
  gameState,
  state => state.currentPage + 1
);
export const getLastPage = createSelector(
  gameState,
  state => Math.floor(state.posts.length / 10) === state.currentPage
);
export const getPosts = createSelector(gameState, state =>
  state.posts.slice(state.currentPage * 10, state.currentPage * 10 + 10)
);
export const getPostByUrl = createSelector(gameState, (state, { url }) =>
  state.posts.find(post => post.url === url)
);
