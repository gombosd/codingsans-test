import { Routes } from '@angular/router';
import { PostDetailedPageComponent } from './post-detailed-page/post-detailed-page.component';

export const postRoutes: Routes = [
  {
    path: '',
    component: PostDetailedPageComponent,
    pathMatch: 'full',
  },
];
