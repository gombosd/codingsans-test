import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { PostDetailedPageComponent } from './post-detailed-page/post-detailed-page.component';
import { SharedModule } from '../shared/shared.module';
import { postRoutes } from './post-detailed-routing.module';

@NgModule({
  imports: [CommonModule, SharedModule, RouterModule.forChild(postRoutes)],
  declarations: [PostDetailedPageComponent],
})
export class PostDetailedModule {}
