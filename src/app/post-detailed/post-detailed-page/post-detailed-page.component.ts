import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';

import { IState, getPostByUrl } from 'src/app/store/reducer';
import { IPost } from 'src/app/store/effects';

@Component({
  selector: 'app-post-detailed-page',
  templateUrl: './post-detailed-page.component.html',
  styleUrls: ['./post-detailed-page.component.css'],
})
export class PostDetailedPageComponent implements OnInit {
  post$: Observable<IPost>;

  constructor(private route: ActivatedRoute, private store: Store<IState>) {}

  ngOnInit() {
    this.getParams();
  }

  onNavigate(url) {
    window.open(url, '_blank');
  }

  private getParams(): void {
    this.route.queryParams.subscribe(({ postUrl }) => this.getPost(postUrl));
  }

  private getPost(url): void {
    this.post$ = this.store.pipe(select(getPostByUrl, { url }));
  }
}
