import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostDetailedPageComponent } from './post-detailed-page.component';

describe('PostDetailedPageComponent', () => {
  let component: PostDetailedPageComponent;
  let fixture: ComponentFixture<PostDetailedPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostDetailedPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostDetailedPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
