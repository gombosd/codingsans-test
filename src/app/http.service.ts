import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IPost } from './store/effects';
@Injectable({
  providedIn: 'root',
})
export class HttpService {
  constructor(private http: HttpClient) {}

  fetchTodos(): Observable<IPost[]> {
    return this.http
      .get('https://www.reddit.com/hot.json')
      .pipe(map((res: any) => res.data.children.map(child => child.data)));
  }
}
