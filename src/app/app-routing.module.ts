import { Routes } from '@angular/router';

export const appRoutes: Routes = [
  {
    path: '',
    loadChildren: './home/home.module#HomeModule',
  },
  {
    path: 'post',
    loadChildren: './post-detailed/post-detailed.module#PostDetailedModule',
  },
];
